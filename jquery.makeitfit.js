/**
 * You know what is annoying and extremely unproductive? When a piece of dynamic content is too long, you do not want to
 * truncate it, AND you do not want to punish the rest of your elements by making their font-size smaller to accommodate.
 * This plugin will make it fit. It works in a similar way to the truncation plugin we wrote before CSS supported auto-
 * truncation of text. It creates an invisible element in the DOM with the same text properties, allowing us to measure
 * the width of the text.
 */
(function($) {
    // Internal list of items that we are making fit.
    var __fitItems = [];

    // These are the types of font-sizes we accept for this plugin
    var __validFontSizeUnits = ['px', 'pt'];

    // These are values that we use to detect if an element is currently floating.
    var __possibleFloatValues = ['left', 'right'];

    // These are the text properties we care about that contribute to the way text looks.
    var __elementTextProps = ['font-size', 'font-family', 'font-weight', 'font-variant', 'letter-spacing', 'font-kerning', 'text-transform', 'color', 'line-height'];

    // Extract the text properties from an element so they can be applied to fake elements.
    var __extractTextProps = function(element) {
        var output = {};

        for(var idx = 0; idx < __elementTextProps.length; idx++)
        {
            var property = __elementTextProps[idx] + '';
            output[property] = element.css(property);
        }

        return output;
    };

    // Basic implementation of nl2br()
    var __nl2br = function(str, is_xhtml) {
        var breakTag = (is_xhtml || typeof is_xhtml === 'undefined') ? '<br />' : '<br>';
        return (str + '').replace(/([^>\r\n]?)(\r\n|\n\r|\r|\n)/g, '$1' + breakTag + '$2');
    };

    // Calculate a specific text's actual height using text properties
    var __calcTextHeight = function(textProps, fontSize, fontSizeUnits, rawTextHTML, textWidth) {
        textWidth = typeof textWidth == 'undefined' ? 0 : parseInt(textWidth, 10);

        // Create the fake element
        var spanElement = $('<span style="display:none; position: absolute;left: 0;top: 0;"></span>');
        spanElement.css(textProps).css('font-size', fontSize + fontSizeUnits);

        // Populate the text
        spanElement.html(rawTextHTML);

        if(textWidth > 0)
            spanElement.css('width', textWidth + 'px');

        // Add the fake element to the DOM to measure
        $('body').append(spanElement);
        var height = parseInt(spanElement.height(), 10);
        spanElement.remove();

        return height;
    };

    // Calculate a single line (or multiple lines by passing numOfLines=X) actual height using text properties
    var __calcTextHeightByLines = function(textProps, fontSize, fontSizeUnits, numOfLines, textWidth) {
        numOfLines = typeof numOfLines == 'undefined' ? 1 : numOfLines;
        textWidth = typeof textWidth == 'undefined' ? 0 : parseInt(textWidth, 10);

        // Create some text that is numOfLines
        var isFirstLine = true;
        var rawText = '';

        for(var idx = 0; idx < numOfLines; idx++)
        {
            if(!isFirstLine)
                rawText += "<br />";

            rawText += "A";
            isFirstLine = false;
        }

        return __calcTextHeight(textProps, fontSize, fontSizeUnits, rawText, textWidth);
    };

    // This function gets called every time the page is re-sized or any other update is generally needed.
    var __makeItFit = function() {
        // Go through each of the items
        $.each(__fitItems, function() {
            var fitItem = this;

            var fitElements = $(fitItem['selector']);
            var fitLines = parseInt(fitItem['lines']);

            fitLines = Math.max(1, fitLines);

            // Now go through each element that matches the selector.
            fitElements.each(function() {
                var fitElement = $(this);
                var fitElementDom = fitElement[0];
                var fitElementTextHTML = fitElement.html();

                // Grab the current font-size value just incase we have to restore it (only happens on errors)
                var originalFontSize = fitElement.css('font-size');

                // Temporarily remove any font-size set by javascript so we can compute the ideal size
                fitElement.css('font-size', '');

                // Compute the style
                var fitComputedStyle = window.getComputedStyle(fitElementDom, null);
                var compFontSize = fitComputedStyle.getPropertyValue('font-size');

                // Take the font-size value and turn it into an int
                var compFontSizeInt = parseInt(compFontSize, 10);

                // Default unit for font-size is pixels
                var compFontSizeUnit = 'px';

                // However if the integer is not zero, then it is possible they supplied a unit.
                if(compFontSizeInt != 0)
                {
                    // Replace non-letters and convert to lowercase
                    compFontSizeUnit = compFontSize.replace(/[^a-zA-Z]/g, '').toLowerCase();
                }

                // Check for invalid font-size units
                if(__validFontSizeUnits.indexOf(compFontSizeUnit) == -1)
                {
                    console.error("Error, invalid unit type specified for font-size. You may only use px or pt with $.makeItFit()... you used:", compFontSizeUnit);
                    fitElement.css('font-size', originalFontSize);
                    return;
                }

                // Now we need to determine the largest size that yields a single line of text for this element.

                // If fitElement is an inline element, inline-block element, or floated, then its parent's width is our measure tool.
                // However, if it is a block element, then we should use its own width.

                var compFloatValue = (fitElement.css('float') || 'none').toLowerCase();
                var isFloating = __possibleFloatValues.indexOf(compFloatValue) != -1;
                var compDisplayValue = $.trim((fitElement.css('display') || 'inline').toLowerCase());

                var idealWidth = -1;

                if(compDisplayValue == 'block')
                {
                    idealWidth = parseInt(fitElement.width(), 10);
                }
                else if(compDisplayValue == 'inline-block'|| compDisplayValue == 'inline' || isFloating)
                {
                    idealWidth = parseInt(fitElement.parent().width(), 10);
                }

                // Spit out an error if we are unable to determine the ideal width.
                if(idealWidth == -1)
                {
                    console.error("Unable to determine the ideal width of fit element. Weird element?");
                    return;
                }

                // Extract all the properties that contribute to the text so we can apply them to our fake elements.
                var calcTextProps = __extractTextProps(fitElement);

                // First we calculate the height of X lines of text.
                var calcSingleLineHeight = __calcTextHeightByLines(calcTextProps, compFontSizeInt, compFontSizeUnit, fitLines, 0);

                // Now let's try projecting the current height
                var calcProjectedHeight = __calcTextHeight(calcTextProps, compFontSizeInt, compFontSizeUnit, fitElementTextHTML, idealWidth);

                // If calcProjectedHeight is small enough already, do nothing!
                if(calcProjectedHeight <= calcSingleLineHeight)
                {
                    return;
                }

                // Otherwise, loop and calculate with smaller font sizes until we find one small enough
                while(calcProjectedHeight > calcSingleLineHeight)
                {
                    compFontSizeInt -= 0.5;

                    if(compFontSizeInt <= 0)
                        break;

                    calcProjectedHeight = __calcTextHeight(calcTextProps, compFontSizeInt, compFontSizeUnit, fitElementTextHTML, idealWidth);
                }

                // Okay, set the font-size now.
                fitElement.css('font-size', compFontSizeInt + compFontSizeUnit);
            });
        });
    };

    // These are the valid operations for the makeItFit function.
    var __validOperations = ['add', 'remove', 'update'];

    $.makeItFit = function(options) {
        var calcOptions = $.extend({
            // These are the defaults.
            'operation': 'update',
            'items': []
        }, options);

        if(typeof calcOptions['operation'] != 'string')
        {
            console.error("Error, you must specify a valid operation for $.makeItFit (either add, remove, or update)");
            return;
        }

        calcOptions['operation'] = calcOptions['operation'].toLowerCase();

        if(__validOperations.indexOf(calcOptions['operation']) == -1)
        {
            console.error("Error, you must specify a valid operation for $.makeItFit (either add, remove, or update)");
            return;
        }

        // Update call is simple, doesn't require any other options.
        if(calcOptions['operation'] == 'update')
        {
            __makeItFit();
            return;
        }

        // Other operations depend heavily on the items.
        if(typeof calcOptions['items'] != 'object')
        {
            console.error("In order to use the add or remove operation for $.makeItFit, you must provide an array of items.");
            return;
        }

        // Create a variable for the items since we're going to loop it below.
        var items = calcOptions['items'];

        // Remove operation is pretty simple. Just use the selector to remove the item from __fitItems
        if(calcOptions['operation'] == 'remove')
        {
            // Start our loop
            $.each(items, function() {
                // The this pointer will be a single item in the array.
                var singleItem = this;

                // Validate the single item to make sure it is an object and spit out an error message if not.
                if(typeof singleItem != 'object')
                {
                    console.error("The following item should be an object. Please see the documentation for more information.", singleItem);
                    return;
                }

                // Validate the single item to make sure they have provided a valid selector and spit out an error message if not.
                if(typeof singleItem['selector'] != 'string')
                {
                    console.error("The selector provided with the item should be string... the following was provided instead.", singleItem['selector']);
                    return;
                }

                // Put the selector in a variable
                var selector = singleItem['selector'];

                // Loop through the items and remove the ones that match the selector.
                for(var idx = 0; idx < __fitItems.length; idx++)
                {
                    if(__fitItems[idx]['selector'] == selector)
                    {
                        __fitItems.splice(idx, 1);
                        idx--;
                    }
                }
            });

            // Call update since we possibly removed one or more items.
            __makeItFit();

            return;
        }

        // The add operation is simple as well. It is add or update, so it should update an existing entry by replacing it.
        if(calcOptions['operation'] == 'add')
        {
            // Start our loop
            $.each(items, function() {
                // The this pointer will be a single item in the array.
                var singleItem = this;

                // Validate the single item to make sure it is an object and spit out an error message if not.
                if(typeof singleItem != 'object')
                {
                    console.error("The following item should be an object. Please see the documentation for more information.", singleItem);
                    return;
                }

                // Validate the single item to make sure they have provided a valid selector and spit out an error message if not.
                if(typeof singleItem['selector'] != 'string')
                {
                    console.error("The selector provided with the item should be string... the following was provided instead.", singleItem['selector']);
                    return;
                }

                // Put the selector in a variable
                var selector = singleItem['selector'];

                // Inherit the default values.
                var calcItem = $.extend({
                    // These are the defaults.
                    'selector': '',
                    'lines': 1
                }, singleItem);

                // Track if this is an update.
                var isUpdate = false;

                // Loop through the items and try to update first.
                for(var idx = 0; idx < __fitItems.length; idx++)
                {
                    if(__fitItems[idx]['selector'] != selector)
                        continue;

                    // Copy properties here. Make sure to edit this if we add more.
                    __fitItems[idx]['selector'] = calcItem['selector'];
                    __fitItems[idx]['lines'] = calcItem['lines'];

                    isUpdate = true;
                    break;
                }

                // If it was an update, we are done.
                if(isUpdate)
                    return;

                // Otherwise, add it.
                __fitItems.push(calcItem);
            });

            // Call update since we possibly removed one or more items.
            __makeItFit();

            return;
        }

        console.error("An unknown error occurred, call the science police.");
    };

    // When the page finishes loading, call update manually.
    $(document).ready(function() {
        __makeItFit();
    });

    // When the window is re-sized, we should call update manually.
    $(window).resize(function() {
        __makeItFit();
    });
})(jQuery);